extends Position3D


export(NodePath) var CONTAINER: NodePath
export(PackedScene) var OBJECT: PackedScene
var time: float = 0.0
var max_time: float = 2.0
const OFFSET_X_LEFT: float = -2.0
const OFFSET_X_RIGHT: float = 2.0


# BUILTINS -------------------------


func _physics_process(delta: float) -> void:
	time += delta
	if time > max_time:
		time = 0.0
		spawn_coin()

# METHODS -------------------------


func spawn_coin() -> void:
	var coin: Area = OBJECT.instance()
	coin.transform.origin.z = self.transform.origin.z
	coin.transform.origin.x = (OFFSET_X_RIGHT if randf() > 0.5 else OFFSET_X_LEFT) + self.transform.origin.x
	var _c: int = coin.connect("coin_collected", get_node(@"/root/Main"), "_on_Coin_collected")
	get_node(CONTAINER).call_deferred("add_child", coin)


# SETGET -------------------------


# SIGNALS -------------------------


