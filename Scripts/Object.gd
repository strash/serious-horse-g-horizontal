extends MeshInstance


const SPEED: float = 12.0
var offset: Vector3 = Vector3.ZERO


# BUILTINS -------------------------


func _ready() -> void:
	self.offset = self.transform.origin


func _physics_process(delta: float) -> void:
	self.offset.z += delta * SPEED
	self.transform.origin.z = self.offset.z
	check_position()


# METHODS -------------------------


func check_position() -> void:
	if self.offset.z > 60.0:
		self.queue_free()


# SETGET -------------------------


# SIGNALS -------------------------


