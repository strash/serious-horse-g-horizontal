extends StaticBody


enum SIDE {
	LEFT,
	RIGHT,
}


# BUILTINS -------------------------


func _ready() -> void:
	($Particles as Particles).emitting = true
	($AnimationPlayer as AnimationPlayer).play("Jiggle")


# METHODS -------------------------


func turn(side: int) -> void:
	if side == SIDE.LEFT:
		self.transform.origin.x = -2.0
	elif side == SIDE.RIGHT:
		self.transform.origin.x = 2.0


# SETGET -------------------------


# SIGNALS -------------------------


