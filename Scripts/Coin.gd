extends Area


signal coin_collected


const ROTATION_ANGLE: float = 0.1
const SPEED: float = 12.0
var offset: Vector3 = Vector3.ZERO
var is_collected: bool = false


# BUILTINS -------------------------


func _ready() -> void:
	self.offset = self.transform.origin


func _physics_process(delta: float) -> void:
	($MeshInstance as MeshInstance).rotate_y(ROTATION_ANGLE)
	self.offset.z += delta * SPEED
	self.transform.origin.z = self.offset.z
	check_position()


# METHODS -------------------------


func check_position() -> void:
	if self.offset.z > 12.0:
		queue_free()


func check_collision(body: Node) -> void:
	if body.name == "Car":
		($MeshInstance as MeshInstance).hide()
		($Particles as Particles).emitting = true
		emit_signal("coin_collected")


# SETGET -------------------------


# SIGNALS -------------------------


func _on_Coin_body_entered(body: Node) -> void:
	if not is_collected:
		($CollisionShape as CollisionShape).disabled = true
		($"." as Area).set_deferred("monitoring", false)
		check_collision(body)
		is_collected = true


