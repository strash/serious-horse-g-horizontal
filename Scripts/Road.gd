extends MeshInstance


const GROUND_SPEED: float = 3.0
var ground_offset_z: float = 0
const ROAD_SPEED: float = 1.8
var road_offset_z: float = 0



# BUILTINS -------------------------


func _physics_process(delta: float) -> void:
	ground_offset_z -= delta * GROUND_SPEED
	road_offset_z -= delta * ROAD_SPEED
	self.mesh.material.uv1_offset.z = ground_offset_z
	($"."/Road as MeshInstance).mesh.material.uv1_offset.z = road_offset_z


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


