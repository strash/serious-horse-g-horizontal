extends Position3D


export(NodePath) var CONTAINER: NodePath
export(Array, PackedScene) var OBJECTS: Array = []
var time: float = 0.0
var max_time: float = 1.0


# BUILTINS -------------------------


func _physics_process(delta: float) -> void:
	self.time += delta
	if self.time > self.max_time:
		self.time = 0.0
		self.max_time = randf()
		spawn_object()


# METHODS -------------------------


func spawn_object() -> void:
	var obj: MeshInstance = OBJECTS[randi() % OBJECTS.size()].instance()
	obj.transform.origin = self.transform.origin
	obj.transform.origin.x = randf() * 15.0 + self.transform.origin.x
	obj.rotate_y(randf() * TAU)
	get_node(CONTAINER).call_deferred("add_child", obj)


# SETGET -------------------------


# SIGNALS -------------------------


