extends Control


signal turn_car


var score: int = 0


# BUILTINS -------------------------


# METHODS -------------------------


func increase_count() -> void:
	score += 1
	($Label as Label).text = str(score)


# SETGET -------------------------


# SIGNALS -------------------------


func _on_ButtonLeft_pressed() -> void:
	emit_signal("turn_car", 0)


func _on_ButtonRight_pressed() -> void:
	emit_signal("turn_car", 1)


