extends Node


# BUILTINS -------------------------


func _ready() -> void:
	randomize()
	var _c: int = ($UI as Control).connect("turn_car", self, "_on_UI_Button_pressed")


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


func _on_Coin_collected() -> void:
	($UI as Control).call_deferred("increase_count")


func _on_UI_Button_pressed(side: int) -> void:
	($Game/Car as StaticBody).call_deferred("turn", side)


